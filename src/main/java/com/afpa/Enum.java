package com.afpa;

/**
 * Classe Enum contenant des énumérations pour les différents types de valeurs possibles pour les attributs d'une classe.
 */
public class Enum {

    /**
     * Énumération pour les valeurs de genre possibles.
     */
    enum GENRE {
        HOMME,
        FEMME
    }

    /**
     * Énumération pour les types de produits possibles.
     */
    enum TYPE {
        CHAUSSURE,
        PULL,
        TSHIRT,
        JEAN,
        SHORT
    }

    /**
     * Énumération pour les tailles possibles.
     */
    enum TAILLE {
        S,
        M,
        L,
        XL,
        XXL,
        XXXL
    }

    /**
     * Énumération pour les couleurs possibles.
     */
    enum COULEUR {
        RED,
        BLUE,
        GREEN,
        WHITE,
        BLACK
    }

    /**
     * Énumération pour les coupe possibles.
     */
    enum COUPER {

        ZIDANE(0),
        MILLITAIRE(3),
        COURT(9),
        MILONG(22),
        LONG(40);

        private Integer longueur;

        COUPER(Integer longueur) {
            this.longueur = longueur;
        }

        //getter de la taille de l'Enum COUPER
        public Integer getLongueur() {

            return longueur;
        }
    }

}
