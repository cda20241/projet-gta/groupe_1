package com.afpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.afpa.Enum.GENRE.FEMME;
import static com.afpa.Enum.GENRE.HOMME;

public class Main {

    public static void main(String[] args) {
        List<Personnage> personnages = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        // Créez un Personnage
        Personnage personnage1 = new Personnage("Jean", HOMME, 180);
        personnages.add(personnage1);
        System.out.println("Liste des personnages apres Personnage1:");
        for (Personnage personnage : personnages) {
            System.out.println("Pseudo : " + personnage.getPseudo());
            System.out.println("Genre : " + personnage.getGenre());
            System.out.println("Taille : " + personnage.getTaille());
            // Vous pouvez ajouter d'autres détails spécifiques au personnage ici
            System.out.println();
        }

        // Créez un Garagiste
        Garagiste garagiste = new Garagiste("BoB", HOMME, 185);

        // Ajoutez le Garagiste à la liste de personnages
        personnages.add(garagiste);
        System.out.println("Liste des personnages apres garagiste:");
        for (Personnage personnage : personnages) {
            System.out.println("Pseudo : " + personnage.getPseudo());
            System.out.println("Genre : " + personnage.getGenre());
            System.out.println("Taille : " + personnage.getTaille());
            // Vous pouvez ajouter d'autres détails spécifiques au personnage ici
            System.out.println();
        }

        // Créez un Coiffeur
        Coiffeur coiffeur = new Coiffeur("Alice", FEMME, 163);

        // Ajoutez le Coiffeur à la liste de personnages
        personnages.add(coiffeur);

        System.out.println("Liste des personnages apres coiffeur1:");
        for (Personnage personnage : personnages) {
            System.out.println("Pseudo : " + personnage.getPseudo());
            System.out.println("Genre : " + personnage.getGenre());
            System.out.println("Taille : " + personnage.getTaille());
            // Vous pouvez ajouter d'autres détails spécifiques au personnage ici
            System.out.println();
        }

        coiffeur.listerCompetences();
        coiffeur.effectuerLeTravail(scanner);
        coiffeur.facturerClient();
        garagiste.listerCompetences();
        garagiste.effectuerLeTravail(scanner);
        garagiste.facturerClient();

    }

}
