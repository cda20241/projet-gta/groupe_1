package com.afpa;

import com.afpa.Enum.COULEUR;

public class Moto extends Vehicule{
    public Moto(String marque, String modele, COULEUR couleur) {
        super(marque, modele, couleur);
    }
    public Moto() {
        super();
    }

}
