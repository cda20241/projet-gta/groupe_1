package com.afpa;

import java.util.Scanner;

public class Coiffeur extends Personnage implements Metier {
    private static final Integer LONGUEUR_CHEVEUX_MAX = 60;
    private Integer longueurCheveuxActuel;
    private Enum.COUPER coupeActuelle;

    public Coiffeur(String pseudo, Enum.GENRE genre, int taille) {
        super(pseudo, genre, taille);
    }

    public Coiffeur(Integer longueurCheveuxActuel) {
        this.longueurCheveuxActuel = longueurCheveuxActuel;
    }

    public Integer getLongueurCheveuxActuel() {
        return longueurCheveuxActuel;
    }

    /**
     * Méthode pour lister les compétences du coiffeur.
     */
    @Override
    public void listerCompetences() {
        System.out.println("Compétences du coiffeur :");
        int index = 1;
        for (Enum.COUPER coupe : Enum.COUPER.values()) {
            System.out.println(index + ". " + coupe.name());
            index++;
        }
        System.out.println(index + ". Poser extensions - Rajouter des extensions de cheveux");
    }

    /**
     * Méthode pour effectuer le travail demandé par le coiffeur.
     * 
     * @param scanner , Scanner permettant de lire la saisie de l'utilisateur.
     */
    @Override
    public void effectuerLeTravail(Scanner scanner) {
        int choix = scanner.nextInt(); // Lire le choix de compétence une seule fois

        if (choix >= 1 && choix <= Enum.COUPER.values().length + 1) {
            if (choix <= Enum.COUPER.values().length) {
                coupeActuelle = Enum.COUPER.values()[choix - 1];
                System.out.println("Le coiffeur réalise la coupe : " + coupeActuelle);
                if (coupeActuelle.getLongueur() > 0) {
                    longueurCheveuxActuel = coupeActuelle.getLongueur();
                }
            } else if (choix == Enum.COUPER.values().length + 1) {
                // Sélection de "Poser extensions"
                System.out.println("Saisissez la longueur des extensions en centimètres : ");
                int longueurExtensions = scanner.nextInt();
                poserExtensions(longueurExtensions);
            }
        } else {
            System.out.println("Option de compétence invalide.");
        }
    }

    /**
     * Méthode pour facturer le client.
     */
    @Override
    public void facturerClient() {
        System.out.println("la facture s'eleve a 100 euro.");

    }

    /**
     * Méthode pour coupe les cheveux du coiffeur.
     */
    public void coupeCheveux() {
        if (longueurCheveuxActuel < LONGUEUR_CHEVEUX_MAX) {
            System.out.println("Le coiffeur n'a pas suffisamment de cheveux pour coupe.");
        } else {
            System.out.println("Le coiffeur commence à coupe les cheveux.");
            // Code pour coupe les cheveux
            longueurCheveuxActuel = 0;
            System.out.println("Le coiffeur a coupe les cheveux.");
        }
    }

    /**
     * Méthode pour poser des extensions de cheveux.
     * 
     * @param longueurExtensions La longueur des extensions à poser en centimètres.
     */
    public void poserExtensions(int longueurExtensions) {
        Integer longueurMaxExtensions = 40;
        if (longueurExtensions > 0 && (longueurCheveuxActuel + longueurExtensions) <= longueurMaxExtensions) {
            longueurCheveuxActuel += longueurExtensions;
            System.out.println("Le coiffeur pose des extensions de " + longueurExtensions + " cm.");

            // Mettre à jour la longueur des cheveux du personnage en utilisant le setter
            setLongueurCheveux(longueurCheveuxActuel);
        } else {
            System.out.println("Impossible de poser autant d'extensions.");
        }
    }

    // getter
    public Integer getLongueurCheveuxMax() {
        return LONGUEUR_CHEVEUX_MAX;
    }

    public Integer getLongueurMinCheveux() {
        return 0;
    }

    public Enum.COUPER getCoupeActuelle() {
        return coupeActuelle;
    }

    public void setCoupeActuelle(Enum.COUPER coupe) {
        this.coupeActuelle = coupe;
    }

}