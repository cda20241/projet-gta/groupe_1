package com.afpa;

import com.afpa.Enum.COULEUR;

public class Bateau extends Vehicule {
    public Bateau(String marque, String modele, COULEUR couleur) {
        super(marque, modele, couleur);
    }
    public Bateau() {
        super();
    }

    @Override
    public void tomberDansLeau() {
        //la methode ne doit pas fonctionner sur la classe Bateau
    }
}
