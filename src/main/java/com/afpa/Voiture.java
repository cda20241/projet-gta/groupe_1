package com.afpa;

import com.afpa.Enum.COULEUR;

public class Voiture extends Vehicule{
    public Voiture(String marque, String modele, COULEUR couleur) {
        super(marque, modele, couleur);
    }
    public Voiture() {
        super();
    }
}