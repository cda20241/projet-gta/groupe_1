package com.afpa;

import java.util.Scanner;

public class Menu {

    static Personnage joueur;
    public static void afficherMenu() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Bienvenue dans le jeu GTA créé par: Dom, Kylian, Melki \uD83D\uDC4B ");
        System.out.println("---------------------------------------------------------");
        System.out.println("1 - Créer un personnage ");
        System.out.println("2 - Habiller personnage");
        System.out.println("3 - Choisir une coupe ");
        System.out.println("4 - Créer une véhicule");
        System.out.println("5 - Réparer votre voiture");
        System.out.println("6 - Quitter le jeux");
        System.out.println("------------------------------------------------------------------");
        System.out.print("Veuillez saisir votre choix :");
        Integer choixMenu = scanner.nextInt();
        switch (choixMenu) {
            case 1:
                System.out.println("---------------------------------------------------------");
                System.out.println("ETAPE 1 - Ensemble, nous allons créer votre personnage ! ");
                System.out.println("---------------------------------------------------------");

                afficherMenu();
                break;
            case 2:
                if (joueur == null) {
                    System.out.println("Vous devez d'abord créer votre personnage !!");
                    afficherMenu();
                } else {
                    System.out.println("---------------------------------------------------------");
                    System.out.println("ETAPE 2 - Choisissons ensemble les vêtements !");
                    System.out.println("---------------------------------------------------------");
                    afficherMenu();
                }
                break;
            case 3:
                System.out.println("---------------------------------------------------------");
                System.out.println("ETAPE 3 - Allons chez le coiffeur pour une jolie coupe ! ");
                System.out.println("---------------------------------------------------------");
                break;
            case 4:
                System.out.println("---------------------------------------------------------");
                System.out.println("ETAPE 4 - Créons ensemble un véhicule ! ");
                System.out.println("---------------------------------------------------------");
                break;
            case 5:
                System.out.println("---------------------------------------------------------");
                System.out.println("ETAPE 5 - Allons chez le garagiste pour réparer la voiture ! ");
                System.out.println("---------------------------------------------------------");
                break;
            case 6:
                System.out.println("---------------------------------------------------------");
                System.out.println("Au revoire ! Merci ! ");
                System.out.println("---------------------------------------------------------");
                break;
            default:
                break;
        }
        scanner.close();
    }




}
