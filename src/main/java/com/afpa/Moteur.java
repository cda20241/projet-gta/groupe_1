package com.afpa;

public class Moteur {
    private Integer pointsDeVie;
    public Moteur() {
        this.pointsDeVie = 100;
    }
    public Integer getPointsDeVie() {
        return pointsDeVie;
    }
    public void setPointsDeVie(Integer pointsDeVie) {
        this.pointsDeVie = pointsDeVie;
    }
}

