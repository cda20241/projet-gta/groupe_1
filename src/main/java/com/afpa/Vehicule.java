package com.afpa;

import com.afpa.Enum.COULEUR;

public class Vehicule {
    // -- ATTRIBUTS --
    private String marque;
    private String modele;
    private COULEUR couleur;
    private Integer pointsDegats;
    private Moteur moteur;
    private Personnage proprietaire;
    private Personnage conducteur;

    // -- CONSTRUCTEUR --

    public Vehicule(String marque, String modele, COULEUR couleur) {
        this.marque = marque;
        this.modele = modele;
        this.couleur = couleur;
        this.pointsDegats = 100;
        this.moteur = new Moteur();
    }

    public Vehicule() {
        this.pointsDegats = 100;
        this.moteur = new Moteur();
    }

    // -- METHODES --

    @Override
    public String toString() {
        return "Le vehicule " + this.getMarque() + " " + this.getModele() + ", PV : " + this.getPointsDegats()
                + ", type : " + this.getClass().getSimpleName();
    }

    public void prendreDegat() {
        this.pointsDegats -= 20;
        int pvMoteur = this.moteur.getPointsDeVie();
        this.moteur.setPointsDeVie(pvMoteur - 20);
        if (this.pointsDegats <= 0) {
            Vehicule v1 = this;
            for (Vehicule v2 : this.proprietaire.getVehicules()) {
                if (v1 == v2) {
                    this.proprietaire.getVehicules().remove(v2);
                }
            }
            v1 = null;
        } else if (this.moteur.getPointsDeVie() <= 0)
            this.moteur.setPointsDeVie(0); // ??
    }

    public void tomberDansLeau() {
        this.moteur.setPointsDeVie(0);
    }

    public boolean demarerVehicule() {
        return this.getPointsDegats() > 0 && this.getMoteur().getPointsDeVie() > 0;
    }

    // pour détruire le vehicule je préfère mettre l'objet à nul qu'utiliser la
    // méthode finalise() qui déconseillée !
    public void ajouterProprietaire(Personnage proprietaire) {
        if (proprietaire != null && !proprietaire.getVehicules().contains(this)) {
            this.setProprietaire(proprietaire);
            this.proprietaire.getVehicules().add(this);
        }
    }

    public void ajouterConducteur(Personnage conducteur) {
        if (conducteur != null) {
            if (conducteur.getConduit() == null && this.getConducteur() == null) {
                conducteur.setConduit(this);
                this.setConducteur(conducteur);
            } else {
                System.out.println("Actions impossible.");
            }
        }
    }

    // -- GETTER & SETTER --

    public String getMarque() {
        return marque;
    }

    public String getModele() {
        return modele;
    }

    public COULEUR getCouleur() {
        return couleur;
    }

    public Integer getPointsDegats() {
        return pointsDegats;
    }

    public Moteur getMoteur() {
        return moteur;
    }

    public Personnage getProprietaire() {
        return proprietaire;
    }

    public Personnage getConducteur() {
        return conducteur;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public void setCouleur(COULEUR couleur) {
        this.couleur = couleur;
    }

    public void setPointsDegats(Integer pointsDegats) {
        this.pointsDegats = pointsDegats;
    }

    public void setMoteur(Moteur moteur) {
        this.moteur = moteur;
    }

    public void setProprietaire(Personnage proprietaire) {
        this.proprietaire = proprietaire;
    }

    public void setConducteur(Personnage conducteur) {
        this.conducteur = conducteur;
    }
}