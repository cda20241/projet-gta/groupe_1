package com.afpa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.afpa.Enum.COULEUR;
import com.afpa.Enum.GENRE;

public class GaragisteV2 extends Personnage implements Metier {

    private final List<String> competences = Arrays.asList("Réparer la carrosserie", "Réparer le moteur",
            "Refaire la peinture");

    public GaragisteV2() {
    }

    public GaragisteV2(String pseudo) {
        super(pseudo);
    }

    public GaragisteV2(String pseudo, GENRE genre, int taille) {
        super(pseudo, genre, taille);
    }

    @Override
    public void listerCompetences() {
        int i = 1;
        System.out.println("Liste des compétences : \nN°0 : retour au menu précédent.");
        for (String competence : competences) {
            System.out.println("compétence n°" + i + " : " + competence);
            i++;
        }
    }

    @Override
    public void effectuerLeTravail(Personnage personnage) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrer une valeur : ");
        Integer index = verifScanner(sc);
        switch (index) {
            case 0:
                // retour menu précédent
                break;
            case 1:
                reparerCarrosserieV2(sc, personnage);
                break;
            case 2:
                reparerMoteurV2(sc, personnage);
                break;
            case 3:
                refairePeintureV2(sc, personnage);
                break;
            default:
                System.out.println("Mauvaise saisie!\brEntrer une valeur correct.");
                effectuerLeTravail(personnage);
                break;
        }
    }

    @Override
    public void facturerClient() {
        System.out.println("La facture est de 100 pesos gringo");
    }

    // __Getteur__//

    public List<String> getCompetences() {
        return this.competences;
    }

    // __Methode__//

    /**
     * Permet à l'utilisateur de choisir un vehicule appartenant à l'instance
     * d'objet de Personnage, puis le revoit.
     * 
     * @param sc
     * @param personnage
     * @return
     */
    public Vehicule choisirVehicule(Scanner sc, Personnage personnage) {
        if (personnage.getVehicules() != null) {
            int i = 1;
            System.out.println("Liste des véhicules : ");
            for (Vehicule vehicule : personnage.getVehicules()) {
                System.out.println("Vehicule n°" + i + " : " + vehicule.toString());
                i++;
            }
            i = 1;
            System.out.println("Entrer le numéro correspondant au véhicule de votre choix : ");
            Integer index = verifScanner(sc);
            for (Vehicule vehicule : personnage.getVehicules()) {
                if (i == index) {
                    return vehicule;
                }
                i++;
            }
        }
        System.out.println("Pas de vehicule.");
        return null;
    }

    /**
     * Remet les pv de l'instance d'objet de Vehicule apparteant à l'objet
     * personnage à 100.
     * 
     * @param sc
     * @param personnage
     */
    public void reparerCarrosserieV2(Scanner sc, Personnage personnage) {
        Vehicule vehicule = choisirVehicule(sc, personnage);
        if (vehicule != null) {
            vehicule.setPointsDegats(100);
            System.out.println("Carroserie réparer.");
        }
    }

    /**
     * Remet les pv de l'objet Moteur de l'instance d'objet de Vehicule de l'objet
     * personnage à 100.
     * 
     * @param sc
     * @param personnage
     */
    public void reparerMoteurV2(Scanner sc, Personnage personnage) {
        Vehicule vehicule = choisirVehicule(sc, personnage);
        if (vehicule != null) {
            vehicule.getMoteur().setPointsDeVie(100);
            System.out.println("Moteur réparer.");
        }
    }

    /**
     * Permet à l'utilisateur de choisir une Couleur parmis les enums, puis de
     * l'appliquer à son Vehicule.
     * 
     * @param sc
     * @param personnage
     */
    public void refairePeintureV2(Scanner sc, Personnage personnage) {
        Vehicule vehicule = choisirVehicule(sc, personnage);
        if (vehicule != null) {
            System.out.println("Choix de couleur : ");
            int i = 0;
            for (COULEUR couleur : COULEUR.values()) {
                System.out.println("N°" + i + " : " + couleur);
            }

            System.out.print("Choisissez une couleur : ");
            Integer index = verifScanner(sc);

            switch (index) {
                case 0:
                    // retour menu précédent
                    break;
                case 1:
                    vehicule.setCouleur(COULEUR.RED);
                    break;
                case 2:
                    vehicule.setCouleur(COULEUR.BLUE);
                    break;
                case 3:
                    vehicule.setCouleur(COULEUR.GREEN);
                    break;
                case 4:
                    vehicule.setCouleur(COULEUR.WHITE);
                    break;
                case 5:
                    vehicule.setCouleur(COULEUR.BLACK);
                    break;
                default:
                    System.out.println("Mauvaise saisie!\brOpération annulée.");
                    break;
            }
        }
    }

    public Integer verifScanner(Scanner sc) {
        if (sc.hasNextInt()) {
            return sc.nextInt();
        } else {
            System.out.println("Aucune entrée disponible.");
            return null;
        }
    }

}
