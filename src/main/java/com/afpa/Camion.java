package com.afpa;

import com.afpa.Enum.COULEUR;

public class Camion extends Vehicule {

    public Camion(String marque, String modele, COULEUR couleur) {
        super(marque, modele, couleur);
    }

    public Camion() {
        super();
    }
}