package com.afpa;

public interface Metier {

    // __Methode__//

    /**
     * Parcours les compétences de la Class implémentant l'interface Metier et les
     * affiches
     */
    void listerCompetences();

    /**
     * Permet à une instance d'une Class implémentant l'interface Metier d'effectuer
     * une de ses compétences sur l'instance d'objet Personnage
     * 
     * @param personnage
     */
    void effectuerLeTravail(Personnage personnage);

    /**
     * Renvoie une valeur String correspondant à une facture
     */
    void facturerClient();
}
