package com.afpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.afpa.Enum.COULEUR;
import com.afpa.Enum.GENRE;
import com.afpa.Enum.TAILLE;
import com.afpa.Enum.TYPE;

public class Personnage {
    // __Attribut__//
    private String pseudo;
    private GENRE genre;
    private Integer taille;
    private Integer longueurCheveux;
    private List<Vetement> vetements;
    private List<Vehicule> vehicules;
    private Vehicule conduit;

    // __Constructeur__//
    public Personnage() {
        this.vetements = new ArrayList<>();
        this.vehicules = new ArrayList<>();
    }

    public Personnage(String pseudo) {
        this.pseudo = pseudo;
        this.vetements = this.choisirVetements();
        this.vehicules = new ArrayList<>();
    }

    public Personnage(String pseudo, GENRE genre, int taille) {
        this.pseudo = pseudo;
        this.genre = genre;
        this.taille = taille;
        if (genre == GENRE.HOMME) {
            this.longueurCheveux = 0;
        } else {
            this.longueurCheveux = 22;
        }
        this.vetements = this.choisirVetements();
        this.vehicules = new ArrayList<>();
    }

    // __Getteur/Setteur__/

    public String getPseudo() {
        return this.pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public GENRE getGenre() {
        return this.genre;
    }

    public void setGenre(GENRE genre) {
        this.genre = genre;
    }

    public Integer getTaille() {
        return this.taille;
    }

    public void setTaille(Integer taille) {
        this.taille = taille;
    }

    public Integer getLongueurCheveux() {
        return this.longueurCheveux;
    }

    public void setLongueurCheveux(Integer longueurCheveux) {
        this.longueurCheveux = longueurCheveux;
    }

    public List<Vetement> getVetements() {
        return this.vetements;
    }

    public void setVetements(List<Vetement> vetements) {
        this.vetements = vetements;
    }

    public List<Vehicule> getVehicules() {
        return this.vehicules;
    }

    public void setVehicules(List<Vehicule> vehicules) {
        this.vehicules = vehicules;
    }

    public Vehicule getConduit() {
        return this.conduit;
    }

    public void setConduit(Vehicule conduit) {
        this.conduit = conduit;
    }

    // __Methode__//

    @Override
    /**
     * Retourne un string constituer des attributs pseudo, genre et sa classe d'une
     * instance Personne.
     * 
     * @return String
     */
    public String toString() {
        return this.getPseudo() + ", " + this.getGenre() + ", " + this.getClass().getSimpleName();
    }

    /**
     * Permet à un Personnage d'avoir accès aux compétences d'une Class implémentant
     * l'interface Metier
     * 
     * @param metier
     */
    public void interagirAvec(Metier metier) {
        metier.listerCompetences();
        metier.effectuerLeTravail(this);
    }

    /**
     * Retourne la taille d'un Personnage avec 10cm en plus si elle porte des
     * chaussures
     * 
     * @return int
     */
    public Integer getTailleTotal() {
        for (Vetement vet : this.getVetements()) {
            if (vet.getType() == TYPE.CHAUSSURE) {
                return this.getTaille() + 10;
            }
        }
        return this.getTaille();
    }

    /**
     * Permet de changer la liste de Vetement rattaché à un personnage
     * 
     * @return List<Vetement>
     */
    public List<Vetement> choisirVetements() {
        Scanner scanner = new Scanner(System.in);
        List<Vetement> listVetements = new ArrayList<>();
        if (this.vetements != null) {
            listVetements = this.vetements;
        }
        for (TYPE type : TYPE.values()) {
            System.out.println("Type de vêtement : " + type);
            Vetement vetement = choisirVetement(scanner, type);
            if (vetement != null && !this.containsSameType(vetement)) {
                listVetements.add(vetement);
            } else {
                for (Vetement ref : listVetements) {
                    if (ref.getType() == type) {
                        listVetements.remove(ref);
                        listVetements.add(vetement);
                    }
                }
            }
        }
        return listVetements;
    }

    /**
     * Crée une instance d'objet de Vetement avec un TYPE pré-intégré, puis permet à
     * l'utilisateur de choisir sa couleur et sa taille
     * 
     * @param scanner
     * @param type
     * @return Vetement
     */
    public Vetement choisirVetement(Scanner scanner, TYPE type) {
        Vetement vetement = new Vetement();
        vetement.setType(type);
        vetement.setCouleur(choisirType(scanner, COULEUR.values()));
        if (vetement.getCouleur() == null) {
            return null;
        }
        vetement.setTaille(choisirType(scanner, TAILLE.values()));
        if (vetement.getTaille() == null) {
            return null;
        }
        return vetement;
    }

    /**
     * Permet de choisir une valeur dans les Enums
     * 
     * @param <T>     type variable
     * @param scanner
     * @param values
     * @return Enum.T
     */
    private <T> T choisirType(Scanner scanner, T[] values) {
        int i = 1;
        System.out.println("Sélectionnez une option :");
        for (T value : values) {
            System.out.println("Option n°" + i + " : " + value);
            i++;
        }
        int choix = scanner.nextInt();
        if (choix == 0) {
            return null;
        } else {
            return values[choix - 1];
        }
    }

    /**
     * Vérifie dans la liste vetement du personnage si le type de vetement est déjà
     * présent dans la liste.
     * Si oui return true
     * Sinon return false
     * 
     * @param vetement
     * @return boolean
     */
    public boolean containsSameType(Vetement vetement) {
        if (this.vetements != null) {
            for (Vetement ref : this.vetements) {
                if (ref.getType() == vetement.getType()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Attribut l'objet Vehicule au Personnage dans sa Liste<Vehicule> et attribut
     * le Personnage à l'attribut propriétaire de Vehicule
     * 
     * @param vehicule
     */
    public void acheterVehicule(Vehicule vehicule) { // manque fonctions pour choisir qu'elle voiture acheter
        if (!this.getVehicules().contains(vehicule)) {
            if (vehicule.getProprietaire() == null) {
                vehicule.setProprietaire(this);
                this.getVehicules().add(vehicule);
            } else {
                System.out.println("Le vehicule à déjà une propriétaire.");
            }
        }
    }

    /**
     * Supprime une voiture de la list<Vehicule> de Personnage et inversement
     * @param vehicule
     */
    public void vendreVehicule(Vehicule vehicule) {
        this.getVehicules().remove(vehicule);
        vehicule.setProprietaire(null);
    }

    /**
     * Attribut l'objet Vehicule au Personnage dans l'attribut conduit et attribut
     * le Personnage à l'attribut conducteur de Vehicule
     * @param vehicule
     */
    public void monterDansVehicule(Vehicule vehicule) {
        if (vehicule != null) {
            if (vehicule.getConducteur() == null) {
                vehicule.setConducteur(this);
                this.setConduit(vehicule);
            } else {
                System.out.println("Il y a déjà un conducteur.");
            }
        }
    }

    /**
     * Supprime la relation conduire/estConduit entre le Vehicule et le Personnage 
     * @param vehicule
     */
    public void descendreDuVehicule(Vehicule vehicule) {
        if (vehicule != null) {
            if (vehicule.getConducteur() == this) {
                vehicule.setConducteur(null);
                this.setConduit(null);
            } else {
                System.out.println("Vous ne pouvez pas descendre si vous n'êtes pas conducteur.");
            }
        }
    }
}
