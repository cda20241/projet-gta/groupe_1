package com.afpa;

import org.reflections.Reflections;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.afpa.Enum.COULEUR;
import com.afpa.Enum.GENRE;

public class World {

    // __Attribut__//

    private List<Personnage> allPersonnages;
    private List<Vehicule> allVehicules;

    // __Constructeur__//

    public World() {
        this.allPersonnages = new ArrayList<>();
        this.allVehicules = new ArrayList<>();
    }

    // __Getteur/Setteur__//

    public List<Personnage> getAllPersonnages() {
        return this.allPersonnages;
    }

    public void setAllPersonnages(List<Personnage> allPersonnages) {
        this.allPersonnages = allPersonnages;
    }

    public List<Vehicule> getAllVehicules() {
        return this.allVehicules;
    }

    public void setAllVehicules(List<Vehicule> allVehicules) {
        this.allVehicules = allVehicules;
    }

    // __Methode__//

    /**
     * Liste toutes les instances de Personnage crée dans world
     */
    public void listerAllPersonnages() {
        int i = 1;
        System.out.println("Liste de tous les personnages :");
        for (Personnage personnage : this.allPersonnages) {
            System.out.println("N°" + i + " : " + personnage.toString());
            i++;
        }
    }

    /**
     * Liste toutes les instances de Vehicule crée dans world
     */
    public void listerAllVehicules() {
        int i = 0;
        System.out.println("Liste de tous les véhicules :");
        for (Vehicule vehicule : this.allVehicules) {
            System.out.println("N°" + i + " : " + vehicule.toString());
            i++;
        }
    }

    /**
     * Ajoute une instance de Personnage dans l'attribut allPersonnages
     * 
     * @param personnage
     */
    public void addPersonnage(Personnage personnage) {
        List<Personnage> sub = this.getAllPersonnages();
        sub.add(personnage);
        this.setAllPersonnages(sub);
    }

    /**
     * Ajoute une instance de Vehicule dans l'attribut allVehicules
     * 
     * @param vehicule
     */
    public void addVehicule(Vehicule vehicule) {
        List<Vehicule> sub = this.getAllVehicules();
        sub.add(vehicule);
        this.setAllVehicules(sub);
    }

    /**
     * Créer une instance de Personnage avec l'attribution d'un pseudo, un genre et
     * de sa taille ainsi que son métier par l'user
     * 
     * @param sc
     */
    public void creerPersonnage(Scanner sc) {
        System.out.println("Choisissez un pseudo : ");
        String pseudo = sc.nextLine();
        System.out.println("Choisissez un genre :\n1 -> Homme\n2 -> Femme");
        int idGenre = sc.nextInt();
        GENRE genre;
        switch (idGenre) {
            case 1:
                genre = GENRE.HOMME;
                break;
            case 2:
                genre = GENRE.FEMME;
                break;
            default:
                genre = null;
                break;
        }
        System.out.println("Choisissez la taille en cm: ");
        int taille = sc.nextInt();
        try {
            Class<?> metier = Class.forName(this.choisirMetier(sc));
            Class<?>[] paramTypes = { String.class, GENRE.class, int.class };
            Object[] paramValues = { pseudo, genre, taille };
            Personnage newPersonnage = (Personnage) metier.getConstructor(paramTypes).newInstance(paramValues);
            this.addPersonnage(newPersonnage);
            System.out.println("Personnage crée.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        sc.close();
    }

    /**
     * Créer une instance de Vehicule avec l'attribution d'une marque, d'une modèle
     * et de sa couleur
     * 
     * @param sc
     */
    public void creerVehicules(Scanner sc) {
        System.out.println("Choisissez la marque de votre voiture : ");
        String marque = sc.nextLine();
        System.out.println("Choisissez le modèle : ");
        String modele = sc.nextLine();
        System.out.println("Choisissez une des couleurs suivante : ");
        int i = 1;
        for (COULEUR couleur : COULEUR.values()) {
            System.out.println("N°" + i + " : " + couleur);
            i++;
        }
        int idCouleur = sc.nextInt();
        COULEUR couleurChoisi = null;
        i = 0;
        for (COULEUR couleur : COULEUR.values()) {
            if (i == idCouleur) {
                couleurChoisi = couleur;
            }
            i++;
        }
        this.addVehicule(new Vehicule(marque, modele, couleurChoisi));

    }

    /**
     * Permet de choisir une instance de Personnage dans l'attribut allPersonnages
     * de world
     * 
     * @return Personnage || null
     */
    public Personnage choisirPersonnage() {
        listerAllPersonnages();
        Scanner sc = new Scanner(System.in);

        System.out.println("Choisissez votre personnage : ");
        int idPersonnage = sc.nextInt();
        int i = 1;

        for (Personnage personnage : this.getAllPersonnages()) {
            if (i == idPersonnage) {
                sc.close();
                return personnage;
            }
        }
        sc.close();
        return null;
    }

    /**
     * Permet de choisir une instance de Vehicule dans l'attribut allVehicules de
     * world
     * 
     * @return
     */
    public Vehicule choisirVehicule() {
        listerAllVehicules();
        Scanner sc = new Scanner(System.in);

        System.out.println("Choisissez votre vehicule : ");
        int idVehicule = sc.nextInt();
        int i = 1;

        for (Vehicule vehicule : this.getAllVehicules()) {
            if (i == idVehicule) {
                sc.close();
                return vehicule;
            }
        }
        sc.close();
        return null;
    }

    /**
     * Retourne un tableau à valeur unique de Class possédant une implémentation de
     * l'interface métier
     * 
     * @return
     */
    public Set<Class<? extends Metier>> listerMetier() {
        Reflections reflections = new Reflections("com.afpa");
        Set<Class<? extends Metier>> implementingClasses = reflections.getSubTypesOf(Metier.class);
        int i = 1;
        System.out.println("L'ensemble des métier :");
        for (Class<? extends Metier> clazz : implementingClasses) {
            System.out.println("Métier n°" + i + " : " + clazz.getSimpleName());
            i++;
        }
        return implementingClasses;
    }

    /**
     * Permet à l'utilisateur de choisir un Class implémentant l'interface Metier
     * 
     * @param sc
     * @return string de la Class choisie
     */
    public String choisirMetier(Scanner sc) {
        int i = 1;
        Set<Class<? extends Metier>> liste = listerMetier();
        System.out.println("Choisir un métier : ");
        int indexMetier = sc.nextInt();
        for (Class<? extends Metier> clazz : liste) {
            if (i == indexMetier) {
                return clazz.getName();
            }
            i++;
        }
        return "Personnage";
    }
}