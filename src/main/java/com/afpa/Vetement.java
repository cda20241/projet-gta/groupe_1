package com.afpa;

import com.afpa.Enum.TYPE;
import com.afpa.Enum.COULEUR;
import com.afpa.Enum.TAILLE;

public class Vetement {

    // __Attribut__//

    private TYPE type;
    private COULEUR couleur;
    private TAILLE taille;

    // __Constructeur__//

    public Vetement() {
        this.type = null;
        this.couleur = null;
        this.taille = null;
    }

    // __getteur/setteur__//

    public TYPE getType() {
        return this.type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public COULEUR getCouleur() {
        return this.couleur;
    }

    public void setCouleur(COULEUR couleur) {
        this.couleur = couleur;
    }

    public TAILLE getTaille() {
        return this.taille;
    }

    public void setTaille(TAILLE taille) {
        this.taille = taille;
    }

}
