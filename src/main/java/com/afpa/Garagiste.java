package com.afpa;

import java.util.List;
import java.util.Scanner;

public class Garagiste extends Personnage implements Metier {
    private Integer pointsDeVieCarrosserie;

    public Garagiste(String pseudo, Enum.GENRE genre, int taille) {
        super(pseudo, genre, taille);
    }

    /**
     * Méthode pour lister les compétences du garagiste.
     */
    @Override
    public void listerCompetences() {

        System.out.println("Compétences du garagiste :");
        System.out.println("1. Réparer la carrosserie");
        System.out.println("2. Réparer le moteur");
        System.out.println("3. Refaire la peinture");
    }

    /**
     * Méthode pour effectuer le travail demandé par le garagiste.
     * 
     * @param scanIndexCompetence L'index de la compétence choisie de l'utilisateur.
     */
    @Override
    public void effectuerLeTravail(Scanner scanIndexCompetence) {
        System.out.println("Sélectionnez le véhicule à réparer :");
        Personnage personnage = this; // Le garagiste travaille sur les véhicules du personnage actuel // this ne peut
                                      // être qu'un object Garagiste !!function mauvais!!
        for (int i = 0; i < personnage.getVehicules().size(); i++) {
            Vehicule vehicule = personnage.getVehicules().get(i);
            System.out.println(i + ". " + vehicule.getMarque() + " " + vehicule.getModele());
            System.out.println(i + "Etat Carroserie : " + vehicule.getPointsDegats() + ", Etat Moteur : "
                    + vehicule.getMoteur().getPointsDeVie());
        }

        int indexVehicule = scanIndexCompetence.nextInt();
        Vehicule vehicule = choisirVehicule(personnage, indexVehicule);

        if (vehicule != null) {
            listerCompetences();
            System.out.println("Sélectionnez la compétence à effectuer :");
            int indexCompetence = scanIndexCompetence.nextInt();
            switch (indexCompetence) {
                case 1:
                    reparerCarrosserie(vehicule);
                    break;
                case 2:
                    reparerMoteur(vehicule);
                    break;
                case 3:
                    refairePeinture(vehicule, scanIndexCompetence);
                    break;
                default:
                    System.out.println("Index de compétence invalide.");
                    break;
            }
        }
    }

    /**
     * Méthode pour facturer le client.
     */
    @Override
    public void facturerClient() {
        if (pointsDeVieCarrosserie == 0) {
            System.out.println("il n'y a pas de facturation");
        } else {
            System.out.println("la facture s'eleve a 1000 euro");
        }
    }

    /**
     * Méthode pour réparer la carrosserie du véhicule.
     */
    private void reparerCarrosserie(Vehicule vehicule) {
        if (vehicule.getPointsDegats() > 0) {
            System.out.println(
                    "Le garagiste répare la carrosserie de " + vehicule.getMarque() + " " + vehicule.getModele());
            vehicule.setPointsDegats(100);// Mise à jour des points de vie de la carrosserie via le setter
        } else {
            System.out.println("La carrosserie de " + vehicule.getMarque() + " " + vehicule.getModele()
                    + " est déjà en parfait état.");
        }
    }

    /**
     * Méthode pour réparer le moteur du véhicule.
     */
    private void reparerMoteur(Vehicule vehicule) {
        Moteur moteur = vehicule.getMoteur();
        if (moteur != null && vehicule.getPointsDegats() > 0 && moteur.getPointsDeVie() != 100) {
            System.out.println("Le garagiste répare le moteur de " + vehicule.getMarque() + " " + vehicule.getModele());
            moteur.setPointsDeVie(100); // Mise à jour des points de vie du moteur via le setter
        } else {
            if (moteur == null) {
                System.out.println("Le véhicule " + vehicule.getMarque() + " " + vehicule.getModele()
                        + " n'a pas de moteur à réparer.");
            } else {
                System.out.println("Le moteur du véhicule " + vehicule.getMarque() + " " + vehicule.getModele()
                        + " est déjà en parfait état.");
            }
        }
    }

    /**
     * Méthode pour refaire la peinture du véhicule.
     */
    private void refairePeinture(Vehicule vehicule, Scanner scanner) {
        // Refaire la peinture
        System.out.println("Sélectionnez la couleur de peinture : ");
        Enum.COULEUR[] couleurs = Enum.COULEUR.values();
        for (Integer i = 0; i < couleurs.length; i++) {
            System.out.println(i + ". " + couleurs[i].name());
        }

        int choixCouleur = scanner.nextInt();

        if (choixCouleur >= 0 && choixCouleur < couleurs.length) {
            Enum.COULEUR nouvelleCouleur = couleurs[choixCouleur];
            System.out.println("Le garagiste refait la peinture de " + vehicule.getMarque() + " " + vehicule.getModele()
                    + " en couleur " + nouvelleCouleur.name());
            vehicule.setCouleur(nouvelleCouleur);
        } else {
            System.out.println("Choix de couleur invalide.");
        }
        scanner.close();
    }


    public Vehicule choisirVehicule(Personnage personnage, Integer index) {
        List<Vehicule> vehicules = personnage.getVehicules();
        if (index >= 0 && index < vehicules.size()) {
            return vehicules.get(index);
        } else {
            System.out.println("Index de véhicule invalide.");
            return null;
        }
    }


}